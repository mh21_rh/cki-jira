"""Tests for the description module."""
import typing
from unittest import TestCase

from kwf_lib import description
from kwf_lib.common import BugzillaID
from kwf_lib.common import CveID
from kwf_lib.common import JiraKey

TAGS = description.Description.TAGS


class LineTest(typing.NamedTuple):
    """Values for a line."""

    raw_line: str
    tag: description.Tag | None
    matching_line: str | None
    match_value: typing.Any


TEST_LINES = [
    LineTest(
        raw_line='Bugzilla: https://bugzilla.redhat.com/111    ',
        tag=TAGS['bugzilla'],
        matching_line='https://bugzilla.redhat.com/111',
        match_value=BugzillaID('111')
    ),
    LineTest(
        raw_line='Bugzilla: http://bugzilla.redhat.com/show_bug.cgi?id=222',
        tag=TAGS['bugzilla'],
        matching_line='http://bugzilla.redhat.com/show_bug.cgi?id=222',
        match_value=BugzillaID('222')
    ),
    LineTest(
        raw_line='Bugzilla: 333      ',
        tag=TAGS['bugzilla'],
        matching_line='333',
        match_value=BugzillaID('333')
    ),
    LineTest(
        raw_line='Bugzilla: hello',
        tag=TAGS['bugzilla'],
        matching_line='hello',
        match_value=None
    ),
    LineTest(
        raw_line='Bugzilla: https://bugzilla.redhat.com/66666 \\',
        tag=TAGS['bugzilla'],
        matching_line='https://bugzilla.redhat.com/66666 \\',
        match_value=None
    ),
    LineTest(
        raw_line='Bug: https://bugzilla.redhat.com/546363',
        tag=None,
        matching_line=None,
        match_value=None
    ),
    LineTest(
        raw_line=' Bugzilla: https://bugzilla.redhat.com/66666 \\',
        tag=None,
        matching_line=None,
        match_value=None
    ),
    LineTest(
        raw_line='Y-Bugzilla: http://bugzilla.redhat.com/show_bug.cgi?id=1234',
        tag=TAGS['y_bugzilla'],
        matching_line='http://bugzilla.redhat.com/show_bug.cgi?id=1234',
        match_value=BugzillaID('1234')
    ),
    LineTest(
        raw_line='Z-Bugzilla: 55555     ',
        tag=TAGS['z_bugzilla'],
        matching_line='55555',
        match_value=BugzillaID('55555')
    ),
    LineTest(
        raw_line='Cc: <example@example.com>   ',
        tag=TAGS['cc'],
        matching_line='<example@example.com>',
        match_value=(None, 'example@example.com')
    ),
    LineTest(
        raw_line='CC: Example User <user@example.com>   ',
        tag=TAGS['cc'],
        matching_line='Example User <user@example.com>',
        match_value=('Example User', 'user@example.com')
    ),
    LineTest(
        raw_line='CVE: CVE-2054-16177   ',
        tag=TAGS['cve'],
        matching_line='CVE-2054-16177',
        match_value=CveID('CVE-2054-16177')
    ),
    LineTest(
        raw_line='Depends: https://gitlab.com/group/project/-/merge_requests/5678   ',
        tag=TAGS['depends'],
        matching_line='https://gitlab.com/group/project/-/merge_requests/5678',
        match_value=5678
    ),
    LineTest(
        raw_line='JIRA: RHEL-123',
        tag=TAGS['jira'],
        matching_line='RHEL-123',
        match_value=None
    ),
    LineTest(
        raw_line='JIRA: https://issues.redhat.com/browse/RHEL-456   ',
        tag=TAGS['jira'],
        matching_line='https://issues.redhat.com/browse/RHEL-456',
        match_value=JiraKey('RHEL-456')
    ),
    LineTest(
        raw_line='O-JIRA: https://issues.redhat.com/browse/RHEL-333 ',
        tag=TAGS['o_jira'],
        matching_line='https://issues.redhat.com/browse/RHEL-333',
        match_value=JiraKey('RHEL-333')
    ),
    LineTest(
        raw_line='Y-JIRA: https://issues.redhat.com/browse/RHEL-234 ',
        tag=TAGS['y_jira'],
        matching_line='https://issues.redhat.com/browse/RHEL-234',
        match_value=JiraKey('RHEL-234')
    ),
    LineTest(
        raw_line='Z-JIRA: http://issues.redhat.com/browse/RHEL-777 ',
        tag=TAGS['z_jira'],
        matching_line='http://issues.redhat.com/browse/RHEL-777',
        match_value=JiraKey('RHEL-777')
    ),
    LineTest(
        raw_line='MR: http://gitlab.com/group/project/-/merge_requests/567 ',
        tag=TAGS['mr'],
        matching_line='http://gitlab.com/group/project/-/merge_requests/567',
        match_value=567
    ),
    LineTest(
        raw_line='Patchwork-id: 1234567 ',
        tag=TAGS['patchwork_id'],
        matching_line='1234567',
        match_value=1234567
    ),
    LineTest(
        raw_line='Signed-off-by: Kernel Developer <kernel-developer@example.com> ',
        tag=TAGS['signed_off_by'],
        matching_line='Kernel Developer <kernel-developer@example.com>',
        match_value=('Kernel Developer', 'kernel-developer@example.com')
    ),
    LineTest(
        raw_line='Upstream Status: linus ',
        tag=TAGS['upstream_status'],
        matching_line='linus',
        match_value='linus'
    ),
    LineTest(
        raw_line='Upstream status: net-next ',
        tag=TAGS['upstream_status'],
        matching_line='net-next',
        match_value='net-next'
    ),
    LineTest(
        raw_line='Y-Commit: 9b7f1571e16e    ',
        tag=TAGS['y_commit'],
        matching_line='9b7f1571e16e',
        match_value=None
    ),
    LineTest(
        raw_line='Y-Commit: bcf9e6ead8c5dcc33613d634e180352f086aa29f',
        tag=TAGS['y_commit'],
        matching_line='bcf9e6ead8c5dcc33613d634e180352f086aa29f',
        match_value='bcf9e6ead8c5dcc33613d634e180352f086aa29f'
    ),
    LineTest(
        raw_line='Omitted-fix: f901c7278b7622298484906018e7e0e9d4762193 commit title',
        tag=TAGS['omitted_fix'],
        matching_line='f901c7278b7622298484906018e7e0e9d4762193 commit title',
        match_value='f901c7278b7622298484906018e7e0e9d4762193'
    ),
    LineTest(
        raw_line='Omitted-fix: d0cd85e956fe [kernel] fix  ',
        tag=TAGS['omitted_fix'],
        matching_line='d0cd85e956fe [kernel] fix',
        match_value='d0cd85e956fe'
    ),
    LineTest(
        raw_line='Ignore-duplicate: a8fda134f45e758659c7ba94f0dd18ae7e22409b',
        tag=TAGS['ignore_duplicate'],
        matching_line='a8fda134f45e758659c7ba94f0dd18ae7e22409b',
        match_value='a8fda134f45e758659c7ba94f0dd18ae7e22409b'
    ),
    LineTest(
        raw_line='Ignore-duplicate: ef6f5c5b4666',
        tag=TAGS['ignore_duplicate'],
        matching_line='ef6f5c5b4666',
        match_value='ef6f5c5b4666'
    ),
]


class TestTag(TestCase):
    """Tests for description.Tag class."""

    def test_lines(self) -> None:
        """Test the TAGS using the TEST_LINES LineTests."""
        self.assertEqual(len(TAGS), 17, 'Adjust this as Tags if the number of Tags change.')

        text = '\n'.join(line.raw_line for line in TEST_LINES)
        self.assertTrue(len(text) > 0, 'text should not be empty.')

        for tag_name, tag in TAGS.items():
            # LineTests for this Tag.
            test_line_data = [test_line for test_line in TEST_LINES if test_line.tag is tag]

            # Expected Tag.lines() return value for this Tag.
            expected_lines = [line.matching_line for line in test_line_data]

            # Expected Tag.match() return value for this Tag.
            expected_match = \
                {line.match_value for line in test_line_data if line.match_value is not None}

            # Actual Tag.match() return value for this Tag and text.
            tag_match = tag.match(text)

            sub_test_params = {
                'tag_name': tag_name,
                'expected_lines': expected_lines,
                'expected_match': expected_match,
                'tag_match': tag_match
            }

            with self.subTest(**sub_test_params):
                self.assertTrue(len(test_line_data) > 0, 'No tests for this tag!')

                # Make sure the expected Tag.lines() & Tag.match() return values match reality.
                self.assertCountEqual(expected_lines, tag.lines(text))
                self.assertEqual(expected_match, tag_match)

                # The number of Tag.matches() values should equal the number of Tag.match() values.
                self.assertEqual(len(expected_match), len(tag.matches(text)))

                # All the Tag.match() values should be of Tag.type type.
                self.assertTrue(all(isinstance(match_value, tag.type) for match_value in tag_match))

    def test_bugzilla_tags(self) -> None:
        """Confirm the Patterns for the Tag for Bugzilla: has the expected group names defined."""
        tags = [
            TAGS['bugzilla'],
            TAGS['y_bugzilla'],
            TAGS['z_bugzilla']
        ]

        for tag in tags:
            with self.subTest(tag=tag):
                self.assertCountEqual(
                    tag.patterns,
                    (description.BUGZILLA_ID_REGEX, description.BUGZILLA_URL_REGEX)
                )

                self.assertIs(tag.type, BugzillaID)

        self.assertCountEqual(description.BUGZILLA_ID_REGEX.groupindex.keys(), ['id'])

        self.assertCountEqual(
            description.BUGZILLA_URL_REGEX.groupindex.keys(),
            ['scheme', 'hostname', 'id']
        )

    def test_cc_tag(self) -> None:
        """Confirm the Patterns for the Tag for Cc:/CC: has the expected group names defined."""
        tag = TAGS['cc']

        self.assertCountEqual(tag.patterns, (description.CC_REGEX,))
        self.assertIs(tag.type, tuple)

        self.assertCountEqual(description.CC_REGEX.groupindex.keys(), ['name', 'email'])

    def test_cve_tag(self) -> None:
        """Confirm the Patterns for the Tag for CVE: has the expected group names defined."""
        tag = TAGS['cve']

        self.assertCountEqual(tag.patterns, (description.CVE_ID_REGEX,))
        self.assertIs(tag.type, CveID)

        self.assertCountEqual(description.CVE_ID_REGEX.groupindex.keys(), ['id'])

    def test_depends_tag(self) -> None:
        """Confirm the Patterns for the Tag for Depends: has the expected group names defined."""
        tag = TAGS['depends']

        self.assertCountEqual(
            tag.patterns,
            (description.DEPENDS_ID_REGEX, description.GITLAB_MR_REGEX)
        )
        self.assertIs(tag.type, int)

        self.assertCountEqual(description.DEPENDS_ID_REGEX.groupindex.keys(), ['id'])
        self.assertCountEqual(
            description.GITLAB_MR_REGEX.groupindex.keys(),
            ['scheme', 'hostname', 'namespace', 'id']
        )

    def test_jira_tags(self) -> None:
        """Confirm the Patterns for the Tag for JIRA: has the expected group names defined."""
        tags = [
            TAGS['jira'],
            TAGS['o_jira'],
            TAGS['y_jira'],
            TAGS['z_jira']
        ]

        for tag in tags:
            with self.subTest(tag=tag):
                self.assertCountEqual(tag.patterns, (description.JIRA_URL_REGEX,))

                self.assertIs(tag.type, JiraKey)

        self.assertCountEqual(
            description.JIRA_URL_REGEX.groupindex.keys(),
            ['scheme', 'hostname', 'project', 'id']
        )

    def test_mr_tag(self) -> None:
        """Confirm the Patterns for the Tag for MR: has the expected group names defined."""
        tag = TAGS['mr']

        self.assertCountEqual(tag.patterns, (description.GITLAB_MR_REGEX,))
        self.assertIs(tag.type, int)

        self.assertCountEqual(
            description.GITLAB_MR_REGEX.groupindex.keys(),
            ['scheme', 'hostname', 'namespace', 'id']
        )

    def test_patchwork_id_tag(self) -> None:
        """Confirm the Patterns for the Tag for Patchwork-id: is okay."""
        tag = TAGS['patchwork_id']

        self.assertCountEqual(tag.patterns, (description.NUMERIC_REGEX,))
        self.assertIs(tag.type, int)

        self.assertCountEqual(description.NUMERIC_REGEX.groupindex.keys(), ['id'])

    def test_signed_off_by_tag(self) -> None:
        """Confirm the Patterns for the Tag for Signed-off-by: is okay."""
        tag = TAGS['signed_off_by']

        self.assertCountEqual(tag.patterns, (description.SIGNOFF_REGEX,))
        self.assertIs(tag.type, tuple)

        self.assertCountEqual(description.SIGNOFF_REGEX.groupindex.keys(), ['name', 'email'])

    def test_upstream_status_tag(self) -> None:
        """Confirm the Patterns for the Tag for Upstream Status: is okay."""
        tag = TAGS['upstream_status']

        self.assertCountEqual(tag.patterns, (description.STRING_REGEX,))
        self.assertIs(tag.type, str)

        self.assertCountEqual(description.STRING_REGEX.groupindex.keys(), ['id'])

    def test_y_commit_tag(self) -> None:
        """Confirm the Patterns for the Tag for Y-Commit: is okay."""
        tag = TAGS['y_commit']

        self.assertCountEqual(tag.patterns, (description.SHA_FULL_REGEX,))
        self.assertIs(tag.type, str)

        self.assertCountEqual(description.SHA_FULL_REGEX.groupindex.keys(), ['id'])

    def test_omitted_fix_tag(self) -> None:
        """Confirm the Patterns for the Tag for Omitted-fix: is okay."""
        tag = TAGS['omitted_fix']

        self.assertCountEqual(tag.patterns, (description.OMITTED_FIX_REGEX,))
        self.assertIs(tag.type, str)

        self.assertCountEqual(description.OMITTED_FIX_REGEX.groupindex.keys(), ['id', 'title'])

    def test_ignore_duplicate_tag(self) -> None:
        """Confirm the Patterns for the Tag for Ignore-duplicate: is okay."""
        tag = TAGS['ignore_duplicate']

        self.assertCountEqual(tag.patterns, (description.SHA_SHORT_REGEX,))
        self.assertIs(tag.type, str)

        self.assertCountEqual(description.SHA_SHORT_REGEX.groupindex.keys(), ['id'])

    def test_tag_regex(self) -> None:
        """All Tag regex Pattens should begin with ^ and end with $."""
        for tag_name, tag in TAGS.items():
            with self.subTest(tag_name=tag_name, patterns=tag.patterns):
                for pattern in tag.patterns:
                    self.assertTrue(pattern.pattern.startswith('^'))
                    self.assertTrue(pattern.pattern.endswith('$'))


class TestDescription(TestCase):
    """Tests for the description.Description class."""

    def test_instance_attributes(self) -> None:
        """Has the expected attributes."""
        test_description = description.Description('')

        self.assertTrue(hasattr(test_description, 'TAGS'))
        self.assertIs(test_description.internal, False)
        self.assertIs(test_description.rhel_only, False)

        self.assertEqual(len(test_description.TAGS), 17)

        for tag_name in test_description.TAGS:
            self.assertTrue(hasattr(test_description, tag_name))
            self.assertIsInstance(getattr(test_description, tag_name), set)

    def test_new_init(self) -> None:
        """Constructs a new instance with no input params, or None."""
        test_description1 = description.Description('')
        test_description2 = description.Description(None)
        test_description3 = description.Description()

        self.assertTrue(test_description1 == test_description2 == test_description3)

    def test_internal(self) -> None:
        """Set to True when the JIRA: tag has 'INTERNAL'."""
        test_description = description.Description('JIRA: INTERNAL ')
        self.assertIs(test_description.internal, True)

    def test_rhel_only(self) -> None:
        """Set to True when the Upstream Status: tag has some form of `RHEL only` string."""
        test_description = description.Description('Upstream status: RHEL Only')
        self.assertIs(test_description.rhel_only, True)

        test_description = description.Description('Upstream-Status: RHEL-Only')
        self.assertIs(test_description.rhel_only, True)

        test_description = description.Description('Upstream Status: RHEL only')
        self.assertIs(test_description.rhel_only, True)

        test_description = description.Description('Upstream-status: RHEL-only')
        self.assertIs(test_description.rhel_only, True)

    def test_lines(self) -> None:
        """Returns the "raw" lines of the given tag name."""
        test_description = description.Description(
            'Bugzilla: BZ123   \n'
            'Bugzilla: https://bugzilla.redhat.com/432532  \\\n'
            'Bugzilla: \n'
        )

        # An unknown Tag name raises a KeyError.
        with self.assertRaises(KeyError):
            test_description.lines('weird tag name')

        self.assertEqual(
            test_description.lines('bugzilla'),
            ['BZ123', 'https://bugzilla.redhat.com/432532  \\', '']
        )

    def test_matches(self) -> None:
        """Returns the Tag values whose Pattern group values match the kwargs."""
        test_description = description.Description(
            'Depends: https://gitlab.com/groupA/project1/-/merge_requests/123\n'
            'Depends: https://gitlab.com/groupA/project2/-/merge_requests/456\n'
            'Depends: https://gitlab.com/groupB/projectX/-/merge_requests/789\n'
            'Depends: !999\n'
        )

        # An unknown Tag name raises a KeyError.
        with self.assertRaises(KeyError):
            test_description.match('bad tag name')

        # Calling match() without any kwargs should match the instance attribute.
        self.assertEqual(test_description.match('depends'), test_description.depends)

        # Without strict=True set, matches without the given group name are always included.
        self.assertEqual(
            test_description.match('depends', namespace='groupA/project1'),
            {123, 999}
        )

        # With strict=True set, matches without the given group name are always excluded.
        self.assertEqual(
            test_description.match('depends', strict=True, namespace='groupA/project1'),
            {123}
        )


class TestNotesDescription(TestCase):
    """Tests for the NotesDescription class."""

    def test_empty(self) -> None:
        """Has the expected values."""
        test_description = description.NotesDescription(None)

        self.assertIsInstance(test_description.header, str)
        self.assertIsInstance(test_description.notes, description.Description)
        self.assertIsInstance(test_description.commit, description.Description)


# This is taken from gitlab.com/cki-project/kernel-workflow/-/blob/main/tests/test_description.py
class TestMRDescription(TestCase):
    """Tests for the MRDescription dataclass."""

    text = ('Hello\n'
            'THINGS THAT SHOULD MATCH:\n'
            'Bugzilla: https://bugzilla.redhat.com/123456  \n'                         # BZ 123456
            'Bugzilla: http://bugzilla.redhat.com/show_bug.cgi?id=637382\n'            # BZ 637382
            'CVE: CVE-2021-61677\n'                                               # CVE-2021-61677
            'CVE: CVE-1992-15616\n'                                               # CVE-1992-15616
            'Depends: https://bugzilla.redhat.com/262727  \n'                         # Dep 262727
            'Signed-off-by: User Name <user@example.com> \n'  # DCO 'User Name', 'user@example.com'
            'Depends: !737\n'                                                          # Dep !737
            'Cc: <noname@example.com>   \n'
            'Depends: https://gitlab.com/group/project/-/merge_requests/267\n'         # Dep !267
            'Signed-off-by: Artist <artist@example.com>\n'    # DCO 'Artist', 'artist@example.com'
            'hey\nDepends: http://gitlab.com/group1/project/-/merge_requests/321  \n'  # Dep !321
            'THINGS THAT SHOULD NOT MATCH:\n'
            'Cc: Example User <example_user@redhat.com>  \n'
            'Bugzilla: 34567\nBugzilla: BZ-456789\n'
            'Cc: Example <example@fedoraproject.org>\n'
            'Cc: Someone Else <someone_else@example.com>\n'
            'Signed-off-by: example <example@example.com> 😎\n'
            '    Bugzilla: https://bugzilla.redhat.com/23456\n'
            '    Signed-off-by: Person <person@example.com>\n'
            )

    def test_MRDescription_empty(self):
        """Returns as False"""
        test_desc = description.Description('')
        self.assertIs(bool(test_desc), False)
        self.assertEqual(test_desc.bugzilla, set())
        self.assertEqual(test_desc.cc, set())
        self.assertEqual(test_desc.cve, set())
        self.assertEqual(test_desc.depends, set())
        self.assertIs(test_desc.internal, False)

    def test_MRDescription_bugzilla(self):
        """Parses the Bugzilla: tags."""
        test_desc = description.Description(self.text)
        # In the original kernel-workflow test this did not include `34567` since we began
        # requiring the full URL but for historic reasons it must be valid to have a bare ID #.
        self.assertEqual(test_desc.bugzilla, {123456, 34567, 637382})

    def test_MRDescription_cc(self):
        """Parses the Cc: tags."""
        test_desc = description.Description(self.text)
        self.assertEqual(test_desc.cc, {('Example', 'example@fedoraproject.org'),
                                        ('Someone Else', 'someone_else@example.com'),
                                        ('Example User', 'example_user@redhat.com'),
                                        (None, 'noname@example.com')})

    def test_MRDescription_cve(self):
        """Parses the CVE: tags."""
        test_desc = description.Description(self.text)
        self.assertEqual(test_desc.cve, {'CVE-2021-61677', 'CVE-1992-15616'})

    def test_MRDescription_depends_no_namespace(self):
        """Parses the Depends: tags for MR IDs."""
        test_desc = description.Description(self.text)
        # In the original kernel-workflow MRDescription object the `Depends` values filter out
        # *any* entries without a matching namespace so bare references like `!123` are always
        # filtered out. But kwf_lib.Description by default does not do this so it recognizes
        # the `!267` and `!321` lines in the text.
        self.assertEqual(test_desc.depends, {267, 321, 737})

    def test_MRDescription_marked_internal(self):
        """Returns True if Bugzilla: INTERNAL is found, otherwise False."""
        test_desc = description.Description(self.text)
        self.assertIs(test_desc.internal, False)

        text = self.text + '\nJIRA: INTERNAL  \n'
        test_desc = description.Description(text)
        self.assertIs(test_desc.internal, True)

    def test_MRDescription_signoff(self):
        """Parses the Signed-off-by: tags for name/email tuples."""
        test_desc = description.Description(self.text)
        self.assertEqual(test_desc.signed_off_by, {('User Name', 'user@example.com'),
                                                   ('Artist', 'artist@example.com')})


class KmtTags(TestCase):
    """Test cases from kmt.tags.CommitTags."""

    def test_parse_commit_msgs_notes(self):
        """Test we parse Y-JIRA present in notes correctly"""
        with open('tests/assets/kmt_tags/ycommit-notes.msg') as f:
            commit_msgs = f.read()

        tags = description.NotesDescription(commit_msgs)

        self.assertCountEqual(tags.jira, ['RHEL-28179',])
        # this is copied in from the original JIRA tag; i.e. should match
        # tags['jira']
        self.assertCountEqual(tags.z_jira, ['RHEL-28179'])
        self.assertCountEqual(tags.y_jira, ['RHEL-26985'])
        self.assertCountEqual(tags.cve, ['CVE-2021-46915'])

    def test_parse_commit_msgs_tags(self):
        """Test we parse Y-JIRA from commit log (no notes) correctly"""
        with open('tests/assets/kmt_tags/ycommit-tag.msg') as f:
            commit_msgs = f.read()

        tags = description.NotesDescription(commit_msgs)
        self.assertCountEqual(tags.jira, ['RHEL-24201'])
        self.assertCountEqual(tags.y_jira, ['RHEL-21941'])
        self.assertCountEqual(tags.z_jira, ['RHEL-24201'])

    def test_parse_commit_msg_o_jira(self):
        """Test we find the O-JIRA if Y-JIRA doesn't exist"""
        with open('tests/assets/kmt_tags/o-jira.msg') as f:
            commit_msgs = f.read()

        tags = description.NotesDescription(commit_msgs)
        self.assertCountEqual(tags.jira, ['RHEL-47630'])
        self.assertCountEqual(tags.cve, ['CVE-2024-39502'])
        self.assertCountEqual(tags.y_commit, ['e53954c3f569029e71702d5afedc193e1124e18e'])
        self.assertCountEqual(tags.y_jira, ['RHEL-47624'])

        # Now insert a Y-JIRA tag and make sure it overrides
        override = commit_msgs.replace(
            'CVE: CVE-2024-39502',
            'CVE: CVE-2024-39502\nY-JIRA: https://issues.redhat.com/browse/RHEL-1234')
        tags = description.NotesDescription(override)
        self.assertCountEqual(tags.y_jira, ['RHEL-1234'])
