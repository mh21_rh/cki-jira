"""Protocols that represent a tracker issue."""
import typing

from kwf_lib.common import CveID
from kwf_lib.common import FixVersion
from kwf_lib.common import GitlabURL
from kwf_lib.common import IssueID
from kwf_lib.common import TestingStatus
from kwf_lib.common import TrackerID
from kwf_lib.common import TrackerLinks
from kwf_lib.common import TrackerPriority
from kwf_lib.common import TrackerResolution
from kwf_lib.common import TrackerSeverity
from kwf_lib.common import TrackerStatus
from kwf_lib.common import TrackerType
from kwf_lib.common import TrackerUser


class BaseTrackerProtocol(typing.Protocol):
    # pylint: disable=too-few-public-methods
    """A protocol for a basic issue tracker."""

    id: TrackerID
    component: str
    product: str
    type: TrackerType | None
    url: str


class KwfTrackerProtocol(BaseTrackerProtocol, typing.Protocol):
    # pylint: disable=too-few-public-methods
    """A protocol for a Red Hat KWF tracker."""

    description: str
    summary: str

    status: TrackerStatus | None
    resolution: TrackerResolution | None

    priority: TrackerPriority | None
    severity: TrackerSeverity | None

    assignee: TrackerUser | None
    creator: TrackerUser | None
    developer: TrackerUser | None
    qa_contact: TrackerUser | None
    pool_team: str

    sub_components: list[str]

    cve_ids: list[CveID]
    embargoed: bool
    flaw_ids: list[IssueID]
    migrated_id: IssueID | None

    keywords: list[str]
    labels: list[str]

    links: TrackerLinks
    _remote_links: list[str] | None

    @property
    def remote_links(self) -> list[str]:
        """Return the list of remote URLs attached to this tracker."""
        return self._remote_links or []

    @remote_links.setter
    def remote_links(self, values: list[str]) -> None:
        """Set the list of remote URLs attached to this tracker."""
        self._remote_links = values


class KwfFlawProtocol(KwfTrackerProtocol, typing.Protocol):
    # pylint: disable=too-few-public-methods
    """A protocol for a Red Hat CVE Flaw issue tracker."""

    cve_id: CveID
    major_incident: bool


class KwfIssueProtocol(KwfTrackerProtocol, typing.Protocol):
    """A protocol for a Red Hat KWF issue tracker."""

    id: IssueID

    fix_version: FixVersion | None
    affects_versions: list[FixVersion | str]

    fixed_in_build: str
    testing_status: TestingStatus | None
    story_points: int
    commit_hashes: list[str]

    @property
    def is_zstream(self) -> bool | None:
        """Return True if this is a zstream tracker, False if not, and None if no fix_version."""
        return self.fix_version.zstream if self.fix_version else None

    @property
    def is_ystream(self) -> bool | None:
        """Return True if this is a ystream tracker, False if not, and None if no fix_version."""
        return not self.fix_version.zstream if self.fix_version else None

    ystream_tracker: list['KwfIssueProtocol']
    zstream_trackers: list['KwfIssueProtocol']

    testing_tasks: list['KwfIssueProtocol']
    variant_trackers: list['KwfIssueProtocol']

    @property
    def assumed_fix_version(self) -> FixVersion | None:
        """For a CVE related tracker, return the fix_version, or first affects_versions, or None."""
        # Return the fix_version if it is set or if this isn't a CVE-related tracker.
        if self.fix_version or not self.cve_ids:
            return self.fix_version

        # If there is an affects_version and it is a valid FixVersion then return it.
        if affects_version := self.affects_versions[0] if self.affects_versions else None:
            if isinstance(affects_version, FixVersion):
                return affects_version

        return None

    def mr_urls(self, hostname: str = '', namespace: str = '') -> list[GitlabURL]:
        """Return the list merge request GitlabURLs this issue links to."""
        mr_urls: list[GitlabURL] = []

        for url in self.remote_links:
            try:
                mr_url = GitlabURL(url)

            except ValueError:
                continue

            if mr_url.type != 'merge_requests':
                continue
            if hostname and mr_url.hostname != hostname:
                continue
            if namespace and mr_url.namespace != namespace:
                continue

            mr_urls.append(mr_url)

        return mr_urls
